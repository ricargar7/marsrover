public class Rover {
    public int[] position;
    public char direction;

    private char[] cardinalPoints = {'N', 'E', 'S', 'W'};

    public Rover(int[] position, char direction) {
        this.position = position;
        this.direction = direction;
    }

    public void translate(char[] commands) {
        for(char command : commands) {
            switch (command) {
                case 'F':
                    moveForward();
                    break;
                case 'B':
                    moveBackwards();
                    break;
                case 'L':
                    rotateLeft();
                    break;
                case 'R':
                    rotateRight();
                    break;
                default:
                    break;
            }
        }
    }

    private void moveBackwards() {
        switch (this.direction) {
            case 'W':
                this.position[0]++;
                break;
            case 'E':
                this.position[0]--;
                break;
            case 'N':
                this.position[1]--;
                break;
            case 'S':
                this.position[1]++;
                break;
        }
    }

    private void moveForward() {
        switch (this.direction) {
            case 'W':
                this.position[0]--;
                break;
            case 'E':
                this.position[0]++;
                break;
            case 'N':
                this.position[1]++;
                break;
            case 'S':
                this.position[1]--;
                break;
        }
    }

    private void rotateRight() {
        char nextCardinalPoint = this.direction;
        for (int i = 0; i < cardinalPoints.length; i++) {
            if (this.direction == cardinalPoints[i]) {
                if (i == cardinalPoints.length - 1) nextCardinalPoint = cardinalPoints[0];
                else nextCardinalPoint = cardinalPoints[i + 1];
            }
        }
        this.direction = nextCardinalPoint;
    }

    private void rotateLeft() {
        char nextCardinalPoint = this.direction;
        for (int i = 0; i < cardinalPoints.length; i++) {
            if (this.direction == cardinalPoints[i]) {
                if (i == 0) nextCardinalPoint = cardinalPoints[cardinalPoints.length - 1];
                else nextCardinalPoint = cardinalPoints[i - 1];
            }
        }
        this.direction = nextCardinalPoint;
    }
}
