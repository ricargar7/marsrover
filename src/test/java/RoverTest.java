import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

public class RoverTest {

    @Test
    public void roverShouldInitializeOnAGivenPosition() {
        int[] position = {1,4};
        Rover rover = new Rover(position,'N');

        Assert.assertEquals(position,rover.position);
    }

    @Test
    public void roverShouldInitializeOnAGivenDirection() {
        int[] position = {1,4};
        Rover rover = new Rover(position,'N');

        Assert.assertEquals('N',rover.direction);
    }

    @Test
    public void roverShouldMoveForward () {
        int[] position = {1,4};
        int[] expectedPosition = {1,5};
        char[] commands = {'F'};
        Rover rover = new Rover(position,'N');

        rover.translate(commands);

        Assert.assertArrayEquals(expectedPosition,rover.position);
    }
    @Test
    public void roverShouldMoveBackward () {
        int[] position = {1,4};
        int[] expectedPosition = {1,3};
        char[] commands = {'B'};
        Rover rover = new Rover(position,'N');

        rover.translate(commands);

        Assert.assertArrayEquals(expectedPosition,rover.position);
    }

    @Test
    public void roverShouldTurnLeft (){
        int[] position = {1,4};
        char[] commands = {'L'};
        Rover rover = new Rover(position,'N');
        Rover rover_2 = new Rover(position,'E');
        Rover rover_3 = new Rover(position,'S');
        Rover rover_4 = new Rover(position,'W');

        rover.translate(commands);
        rover_2.translate(commands);
        rover_3.translate(commands);
        rover_4.translate(commands);

        Assert.assertEquals('W',rover.direction);
        Assert.assertEquals('N',rover_2.direction);
        Assert.assertEquals('E',rover_3.direction);
        Assert.assertEquals('S',rover_4.direction);
    }

    @Test
    public void roverShouldTurnRight (){
        int[] position = {1,4};
        char[] commands = {'R'};
        Rover rover = new Rover(position,'N');
        Rover rover_2 = new Rover(position,'E');
        Rover rover_3 = new Rover(position,'S');
        Rover rover_4 = new Rover(position,'W');

        rover.translate(commands);
        rover_2.translate(commands);
        rover_3.translate(commands);
        rover_4.translate(commands);

        Assert.assertEquals('E',rover.direction);
        Assert.assertEquals('S',rover_2.direction);
        Assert.assertEquals('W',rover_3.direction);
        Assert.assertEquals('N',rover_4.direction);
    }

    @Test
    public void roverShouldMoveForwardMultipleTimes() {
        int[] position = {1,4};
        int[] expectedPosition = {1,6};
        char[] commands = {'F','F'};
        Rover rover = new Rover(position,'N');

        rover.translate(commands);

        Assert.assertArrayEquals(expectedPosition,rover.position);
    }

    @Test
    public void roverShouldMoveInDifferentDirectionsMultipleTimes() {
        int[] position = {0,0};
        int[] expectedPosition = {-1,0};
        char[] commands = {'F','R','B','R','F'};
        Rover rover = new Rover(position,'N');

        rover.translate(commands);

        Assert.assertArrayEquals(expectedPosition, rover.position);
    }
}

